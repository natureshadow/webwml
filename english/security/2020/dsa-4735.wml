<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the GRUB2 bootloader.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10713">CVE-2020-10713</a>

    <p>A flaw in the grub.cfg parsing code was found allowing to break
    UEFI Secure Boot and load arbitrary code. Details can be found at
    <a href="https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/">https://www.eclypsium.com/2020/07/29/theres-a-hole-in-the-boot/</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14308">CVE-2020-14308</a>

    <p>It was discovered that grub_malloc does not validate the allocation
    size allowing for arithmetic overflow and subsequently a heap-based
    buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14309">CVE-2020-14309</a>

    <p>An integer overflow in grub_squash_read_symlink may lead to a heap    based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14310">CVE-2020-14310</a>

    <p>An integer overflow in read_section_from_string may lead to a heap    based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14311">CVE-2020-14311</a>

    <p>An integer overflow in grub_ext2_read_link may lead to a heap-based
    buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15706">CVE-2020-15706</a>

    <p>script: Avoid a use-after-free when redefining a function during
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15707">CVE-2020-15707</a>

    <p>An integer overflow flaw was found in the initrd size handling.</p></li>
</ul>

<p>Further detailed information can be found at
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot">https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot</a></p>

<p>For the stable distribution (buster), these problems have been fixed in
version 2.02+dfsg1-20+deb10u1.</p>

<p>We recommend that you upgrade your grub2 packages.</p>

<p>For the detailed security status of grub2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/grub2">https://security-tracker.debian.org/tracker/grub2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4735.data"
# $Id: $
