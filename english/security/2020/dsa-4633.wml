<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in cURL, an URL transfer
library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5436">CVE-2019-5436</a>

    <p>A heap buffer overflow in the TFTP receiving code was discovered,
    which could allow DoS or arbitrary code execution. This only affects
    the oldstable distribution (stretch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5481">CVE-2019-5481</a>

    <p>Thomas Vegas discovered a double-free in the FTP-KRB code, triggered
    by a malicious server sending a very large data block.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

    <p>Thomas Vegas discovered a heap buffer overflow that could be
    triggered when a small non-default TFTP blocksize is used.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 7.52.1-5+deb9u10.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 7.64.0-4+deb10u1.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4633.data"
# $Id: $
