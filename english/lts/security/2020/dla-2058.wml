<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that certain cryptographic primitives in nss, the Network
Security Service libraries, did not check the length of the input
text. This could result in a potential heap-based buffer overflow.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2:3.26-1+debu8u10.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2058.data"
# $Id: $
