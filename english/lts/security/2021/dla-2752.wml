<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in squashfs-tools, a tool to create and append to
squashfs filesystems.
As unsquashfs did not validate all filepaths, it would allow writing
outside of the original destination.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1:4.3-3+deb9u2.</p>

<p>We recommend that you upgrade your squashfs-tools packages.</p>

<p>For the detailed security status of squashfs-tools please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squashfs-tools">https://security-tracker.debian.org/tracker/squashfs-tools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2752.data"
# $Id: $
