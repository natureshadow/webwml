<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the OpenJDK Java runtime,
including issues with cyprographic hashing, TLS client handshaking, and
various other issues.</p>

<p>Thanks to Thorsten Glaser and ⮡ tarent for contributing the updated
packages to address these vulnerabilities.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
8u312-b07-1~deb9u1.</p>

<p>We recommend that you upgrade your openjdk-8 packages.</p>

<p>For the detailed security status of openjdk-8 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjdk-8">https://security-tracker.debian.org/tracker/openjdk-8</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2814.data"
# $Id: $
