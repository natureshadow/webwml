<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been identified in the VNC code of iTALC, a
classroom management software. All vulnerabilities referenced below are
issues that have originally been reported against Debian source package
libvncserver. The italc source package in Debian ships a custom-patched
version of libvncserver, thus libvncserver's security fixes required
porting over.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6051">CVE-2014-6051</a>

    <p>Integer overflow in the MallocFrameBuffer function in vncviewer.c in
    LibVNCServer allowed remote VNC servers to cause a denial of service
    (crash) and possibly executed arbitrary code via an advertisement for
    a large screen size, which triggered a heap-based buffer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6052">CVE-2014-6052</a>

    <p>The HandleRFBServerMessage function in libvncclient/rfbproto.c in
    LibVNCServer did not check certain malloc return values, which
    allowed remote VNC servers to cause a denial of service (application
    crash) or possibly execute arbitrary code by specifying a large
    screen size in a (1) FramebufferUpdate, (2) ResizeFrameBuffer, or (3)
    PalmVNCReSizeFrameBuffer message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6053">CVE-2014-6053</a>

    <p>The rfbProcessClientNormalMessage function in
    libvncserver/rfbserver.c in LibVNCServer did not properly handle
    attempts to send a large amount of ClientCutText data, which allowed
    remote attackers to cause a denial of service (memory consumption or
    daemon crash) via a crafted message that was processed by using a
    single unchecked malloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6054">CVE-2014-6054</a>

    <p>The rfbProcessClientNormalMessage function in
    libvncserver/rfbserver.c in LibVNCServer allowed remote attackers to
    cause a denial of service (divide-by-zero error and server crash) via
    a zero value in the scaling factor in a (1) PalmVNCSetScaleFactor or
    (2) SetScale message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-6055">CVE-2014-6055</a>

    <p>Multiple stack-based buffer overflows in the File Transfer feature in
    rfbserver.c in LibVNCServer allowed remote authenticated users to
    cause a denial of service (crash) and possibly execute arbitrary code
    via a (1) long file or (2) directory name or the (3) FileTime
    attribute in a rfbFileTransferOffer message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9941">CVE-2016-9941</a>

    <p>Heap-based buffer overflow in rfbproto.c in LibVNCClient in
    LibVNCServer allowed remote servers to cause a denial of service
    (application crash) or possibly execute arbitrary code via a crafted
    FramebufferUpdate message containing a subrectangle outside of the
    client drawing area.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9942">CVE-2016-9942</a>

    <p>Heap-based buffer overflow in ultra.c in LibVNCClient in LibVNCServer
    allowed remote servers to cause a denial of service (application
    crash) or possibly execute arbitrary code via a crafted
    FramebufferUpdate message with the Ultra type tile, such that the LZO
    payload decompressed length exceeded what is specified by the tile
    dimensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6307">CVE-2018-6307</a>

    <p>LibVNC contained heap use-after-free vulnerability in server code of
    file transfer extension that can result remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7225">CVE-2018-7225</a>

    <p>An issue was discovered in LibVNCServer.
    rfbProcessClientNormalMessage() in rfbserver.c did not sanitize
    msg.cct.length, leading to access to uninitialized and potentially
    sensitive data or possibly unspecified other impact (e.g., an integer
    overflow) via specially crafted VNC packets.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15126">CVE-2018-15126</a>

    <p>LibVNC contained heap use-after-free vulnerability in server code of
    file transfer extension that can result remote code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a>

    <p>LibVNC contained heap out-of-bound write vulnerability in server code
    of file transfer extension that can result remote code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20749">CVE-2018-20749</a>

    <p>LibVNC contained a heap out-of-bounds write vulnerability in
    libvncserver/rfbserver.c. The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a> was incomplete.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20750">CVE-2018-20750</a>

    <p>LibVNC contained a heap out-of-bounds write vulnerability in
    libvncserver/rfbserver.c. The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-15127">CVE-2018-15127</a> was incomplete.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a>

    <p>LibVNC contained multiple heap out-of-bound write vulnerabilities in
    VNC client code that can result remote code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20748">CVE-2018-20748</a>

    <p>LibVNC contained multiple heap out-of-bounds write vulnerabilities in
    libvncclient/rfbproto.c. The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-20019">CVE-2018-20019</a> was incomplete.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20020">CVE-2018-20020</a>

    <p>LibVNC contained heap out-of-bound write vulnerability inside
    structure in VNC client code that can result remote code execution</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20021">CVE-2018-20021</a>

    <p>LibVNC contained a CWE-835: Infinite loop vulnerability in VNC client
    code. Vulnerability allows attacker to consume excessive amount of
    resources like CPU and RAM</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20022">CVE-2018-20022</a>

    <p>LibVNC contained multiple weaknesses CWE-665: Improper Initialization
    vulnerability in VNC client code that allowed attackers to read stack
    memory and could be abused for information disclosure. Combined with
    another vulnerability, it could be used to leak stack memory layout
    and in bypassing ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20023">CVE-2018-20023</a>

    <p>LibVNC contained CWE-665: Improper Initialization vulnerability in
    VNC Repeater client code that allowed attacker to read stack memory
    and could be abused for information disclosure. Combined with another
    vulnerability, it could be used to leak stack memory layout and in
    bypassing ASLR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20024">CVE-2018-20024</a>

    <p>LibVNC contained null pointer dereference in VNC client code that
    could result DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15681">CVE-2019-15681</a>

    <p>LibVNC contained a memory leak (CWE-655) in VNC server code, which
    allowed an attacker to read stack memory and could be abused for
    information disclosure. Combined with another vulnerability, it could
    be used to leak stack memory and bypass ASLR. This attack appeared to
    be exploitable via network connectivity.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:2.0.2+dfsg1-2+deb8u1.</p>

<p>We recommend that you upgrade your italc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1979.data"
# $Id: $
