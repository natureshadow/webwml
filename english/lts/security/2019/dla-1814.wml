<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a cross-site scripting (XSS)
vulnerability in the Django web development framework</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12308">CVE-2019-12308</a>

    <p>An issue was discovered in Django 1.11 before 1.11.21, 2.1 before 2.1.9, and 2.2 before 2.2.2. The clickable Current URL value displayed by the AdminURLFieldWidget displays the provided value without validating it as a safe URL. Thus, an unvalidated value stored in the database, or a value provided as a URL query parameter payload, could result in an clickable JavaScript link.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u5.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1814.data"
# $Id: $
