<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Sylvain Beucler and Dan Walma discovered several directory traversal
issues in DFArc, a frontend and extensions manager for the Dink
Smallwood game, allowing an attacker to overwrite arbitrary files on
the user's system.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.12-1+deb8u1.</p>

<p>We recommend that you upgrade your freedink-dfarc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1686.data"
# $Id: $
