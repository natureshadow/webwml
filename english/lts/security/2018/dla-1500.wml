<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been found in OpenSSH, a free implementation
of the SSH protocol suite:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5352">CVE-2015-5352</a>

    <p>OpenSSH incorrectly verified time window deadlines for X connections.
    Remote attackers could take advantage of this flaw to bypass intended
    access restrictions. Reported by Jann Horn.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5600">CVE-2015-5600</a>

    <p>OpenSSH improperly restricted the processing of keyboard-interactive
    devices within a single connection, which could allow remote attackers
    to perform brute-force attacks or cause a denial of service, in a
    non-default configuration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6563">CVE-2015-6563</a>

    <p>OpenSSH incorrectly handled usernames during PAM authentication. In
    conjunction with an additional flaw in the OpenSSH unprivileged child
    process, remote attackers could make use if this issue to perform user
    impersonation. Discovered by Moritz Jodeit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-6564">CVE-2015-6564</a>

    <p>Moritz Jodeit discovered a use-after-free flaw in PAM support in
    OpenSSH, that could be used by remote attackers to bypass
    authentication or possibly execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1908">CVE-2016-1908</a>

    <p>OpenSSH mishandled untrusted X11 forwarding when the X server disables
    the SECURITY extension. Untrusted connections could obtain trusted X11
    forwarding privileges. Reported by Thomas Hoger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3115">CVE-2016-3115</a>

    <p>OpenSSH improperly handled X11 forwarding data related to
    authentication credentials. Remote authenticated users could make use
    of this flaw to bypass intended shell-command restrictions. Identified
    by github.com/tintinweb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6515">CVE-2016-6515</a>

    <p>OpenSSH did not limit password lengths for password authentication.
    Remote attackers could make use of this flaw to cause a denial of
    service via long strings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10009">CVE-2016-10009</a>

    <p>Jann Horn discovered an untrusted search path vulnerability in
    ssh-agent allowing remote attackers to execute arbitrary local
    PKCS#11 modules by leveraging control over a forwarded agent-socket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10011">CVE-2016-10011</a>

    <p>Jann Horn discovered that OpenSSH did not properly consider the
    effects of realloc on buffer contents. This may allow local users to
    obtain sensitive private-key information by leveraging access to a
    privilege-separated child process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10012">CVE-2016-10012</a>

    <p>Guido Vranken discovered that the OpenSSH shared memory manager
    did not ensure that a bounds check was enforced by all compilers,
    which could allow local users to gain privileges by leveraging access
    to a sandboxed privilege-separation process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10708">CVE-2016-10708</a>

    <p>NULL pointer dereference and daemon crash via an out-of-sequence
    NEWKEYS message.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15906">CVE-2017-15906</a>

    <p>Michal Zalewski reported that OpenSSH improperly prevent write
    operations in readonly mode, allowing attackers to create zero-length
    files.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1:6.7p1-5+deb8u6.</p>

<p>We recommend that you upgrade your openssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1500.data"
# $Id: $
