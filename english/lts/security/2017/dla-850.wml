<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6349">CVE-2017-6349</a>

    <p>An integer overflow at a u_read_undo memory allocation site would occur
    for vim before patch 8.0.0377, if it does not properly validate values
    for tree length when reading a corrupted undo file, which may lead to
    resultant buffer overflows.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6350">CVE-2017-6350</a>

    <p>An integer overflow at an unserialize_uep memory allocation site would
    occur for vim before patch 8.0.0378, if it does not properly validate
    values for tree length when reading a corrupted undo file, which may
    lead to resultant buffer overflows.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:7.3.547-7+deb7u3.</p>

<p>We recommend that you upgrade your vim packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-850.data"
# $Id: $
