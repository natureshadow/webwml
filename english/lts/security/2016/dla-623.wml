<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Dawid Golunski from legalhackers.com discovered that Debian's version
of Tomcat 7 was vulnerable to a local privilege escalation. Local
attackers who have gained access to the server in the context of the
tomcat7 user through a vulnerability in a web application were able to
replace the file with a symlink to an arbitrary file.</p>

<p>The full advisory can be found at</p>

<p><url "http://legalhackers.com/advisories/Tomcat-Debian-based-Root-Privilege-Escalation-Exploit.txt"></p>

<p>In addition this security update also fixes Debian bug #821391. File
ownership in /etc/tomcat7 will no longer be unconditionally overridden
on upgrade. As another precaution the file permissions of Debian
specific configuration files in /etc/tomcat7 were changed to 640 to
disallow world readable access.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u6.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-623.data"
# $Id: $
