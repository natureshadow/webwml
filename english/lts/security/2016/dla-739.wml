<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8691">CVE-2016-8691</a>

      <p>FPE on unknown address ... jpc_dec_process_siz ... jpc_dec.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8692">CVE-2016-8692</a>

      <p>FPE on unknown address ... jpc_dec_process_siz ... jpc_dec.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8693">CVE-2016-8693</a>

      <p>attempting double-free ... mem_close ... jas_stream.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8882">CVE-2016-8882</a>

      <p>segfault / null pointer access in jpc_pi_destroy</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9560">CVE-2016-9560</a>

      <p>stack-based buffer overflow in jpc_tsfb_getbands2 (jpc_tsfb.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8887">CVE-2016-8887</a> part 1 + 2

      <p>NULL pointer dereference in jp2_colr_destroy (jp2_cod.c)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8654">CVE-2016-8654</a>

      <p>Heap-based buffer overflow in QMFB code in JPC codec</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8883">CVE-2016-8883</a>

      <p>assert in jpc_dec_tiledecode()</p>

<li>TEMP-CVE

      <p>heap-based buffer overflow in jpc_dec_tiledecode (jpc_dec.c)</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.900.1-13+deb7u5.</p>

<p>We recommend that you upgrade your jasper packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-739.data"
# $Id: $
