# templates webwml Catalan template.
# Copyright (C) 2002, 2003, 2004, 2005 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002, 2003.
# Guillem Jover <guillem@debian.org>, 2004-2007, 2011, 2017-2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2021-05-11 10:20+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.3\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Lloc web de Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Cercar el lloc web de Debian."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Cerca del lloc web de Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Sí"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "No"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Projecte Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian és un sistema operatiu i una distribució de Programari Lliure. Es "
"manté i actualitza gràcies a la feina de molts usuaris que contribueixen amb "
"el seu treball i esforç."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, lliure, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr ""
"Tornar a la pàgina principal del <a href=\"m4_HOME/\">Projecte Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Pàgina principal"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Evita navegació rapida"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Quant a"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Quant a Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Contacteu amb nosaltres"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Informació legal"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Privacitat de dades"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Donacions"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Esdeveniments"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Notícies"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribució"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Suport"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Mescles pures"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Racó del desenvolupador"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Documentació"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Informació sobre seguretat"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Cerca"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "cap"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Anem-hi!"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "global"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Mapa del lloc"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Miscel·lània"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Obtenir Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "El bloc de Debian"

#: ../../english/template/debian/common_translation.wml:94
#, fuzzy
#| msgid "Debian Project News"
msgid "Debian Micronews"
msgstr "Notícies del projecte Debian"

#: ../../english/template/debian/common_translation.wml:97
#, fuzzy
#| msgid "Debian Project"
msgid "Debian Planet"
msgstr "Projecte Debian"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "Última actualització"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Si us plau, envieu tots els comentaris, suggeriments i crítiques sobre "
"aquesta pàgina a la nostra <a href=\"mailto:debian-doc@lists.debian.org"
"\">llista de correu</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "no cal"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "no disponible"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/D"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "en la versió 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "en la versió 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "en la versió 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "en la versió 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "en la versió 2.2"

#. TRANSLATORS: Please make clear in the translation of the following
#. item that mail sent to the debian-www list *must* be in English. Also,
#. you can add some information of your own translation mailing list
#. (i.e. debian-l10n-XXXXXX@lists.debian.org) for reporting things in
#. your language.
#: ../../english/template/debian/footer.wml:89
msgid ""
"To report a problem with the web site, please e-mail our publicly archived "
"mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-www@lists."
"debian.org</a> in English.  For other contact information, see the Debian <a "
"href=\"m4_HOME/contact\">contact page</a>. Web site source code is <a href="
"\"https://salsa.debian.org/webmaster-team/webwml\">available</a>."
msgstr ""
"Per informar d'algun problema amb el lloc web, envieu un correu en anglès a "
"la llista de correu arxivada públicament <a href=\"mailto:debian-www@lists."
"debian.org\">debian-www@lists.debian.org</a> en anglès. Per a altre "
"informació de contacte, vegeu la <a href=\"m4_HOME/contact\">pàgina de "
"contacte</a> de Debian. El codi font del lloc web està <a href=\"https://"
"salsa.debian.org/webmaster-team/webwml\">disponible</a>."

#: ../../english/template/debian/footer.wml:92
msgid "Last Modified"
msgstr "Última modificació"

#: ../../english/template/debian/footer.wml:95
msgid "Last Built"
msgstr "Última construcció"

#: ../../english/template/debian/footer.wml:98
msgid "Copyright"
msgstr "Copyright"

#: ../../english/template/debian/footer.wml:101
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> i altres;"

#: ../../english/template/debian/footer.wml:104
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Vegeu <a href=\"m4_HOME/license\" rel=\"copyright\">els termes de la "
"llicència</a>"

#: ../../english/template/debian/footer.wml:107
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian és una <a href=\"m4_HOME/trademark\">marca</a> registrada de Software "
"in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Aquesta pàgina també està disponible en els següents idiomes:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Com configurar <a href=m4_HOME/intro/cn>l'idioma per defecte als documents</"
"a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "Navegador predeterminat"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "Eliminar la galeta de substitució de llenguatge"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian internacionalment"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Socis"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Notícies setmanals de Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Notícies setmanals"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Notícies del projecte Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Notícies del projecte"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informació sobre versions"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Paquets Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Baixar"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;en&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Llibres sobre Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "El wiki de Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Arxius de llistes de correu"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Llistes de correu"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Contracte social"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Codi de conducta"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - El sistema operatiu universal"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Mapa de les pàgines al lloc de Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Base de dades dels desenvolupadors"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "PMF de Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Manual de normes de Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Referència per a desenvolupadors de Debian"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Guia per a nous Desenvolupadors"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Errors crítics per a la publicació de noves versions"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Informes de Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Arxius de llistes de correu per a usuaris"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Arxius de llistes de correu per a desenvolupadors"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Arxius de llistes de correu per a i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Arxius de llistes de correu per als ports"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Arxius de llistes de correu del sistema de seguiment d'errors"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Arxius de llistes de correu variades"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Programari lliure"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Desenvolupament"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Ajudeu a Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Informes d'error"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Ports/Arquitectures"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Manual d'instal·lació"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Venedors de CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Imatges ISO de CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Instal·lació per xarxa"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Preinstal·lat"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Projecte Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Salsa &ndash; Gitlab de Debian"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Control de qualitat"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Sistema de seguiment de paquets"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Inspecció rapida dels paquets dels desenvolupadors de Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Pàgina d'inici de Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Cap element per aquest any."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "proposada"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "en discussió"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "votació oberta"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "acabat"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "retirada"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Esdeveniments futurs"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Esdeveniments passats"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nova revisió)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Informe"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "Pàgina redirigida a <newpage/>"

#: ../../english/template/debian/redirect.wml:12
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"Aquesta pàgina s'ha reanomenat com <url <newpage/>>, actualitzeu els vostres "
"enllaços."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s per a %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Nota:</em> El <a href=\"$link\">document original</a> és més nou que "
"aquesta traducció."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Avís! Aquesta traducció és massa antiga, vegeu <a href=\"$link\">l'original</"
"a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr ""
"<em>Nota:</em> El document original d'aquesta traducció ja no existeix."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "La versió de traducció és incorrecta!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Tornar a la <a href=\"./\">pàgina de Qui fa servir Debian?</a>."

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s  &ndash; %s, versió %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s  &ndash; %s: %s"

#~ msgid "%s days in adoption."
#~ msgstr "%s dies en adopció."

#~ msgid "%s days in preparation."
#~ msgstr "%s dies en preparació."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "Els <a href=\"../../\">números anteriors</a> d'aquest butlletí estan "
#~ "disponibles."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (enllaç trencat)"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Coordinador del</th><th>Projecte</th>"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Treball artístic"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Descàrrega"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Replicant"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Misc"

#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "<void id=\"dc_pik\" />Kit de pseudo imatge"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Informació de llançament d'imatge"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Repliques rsync"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Descàrrega amb Torrent"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />pmf"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />misc"

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Els editors de les notícies setmanals de Debian són "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Els editors de les notícies setmanals de Debian són "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Ha estat traduït per %s."

#, fuzzy
#~| msgid ""
#~| "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~| "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Els editors d'aquest número de les notícies "
#~ "setmanals de Debian són <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Els editors d'aquest número de les notícies "
#~ "setmanals de Debian són <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Ha estat traduït per %s."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~| "dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />L'editor de les notícies setmanals de Debian és "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />L'editor de les notícies setmanals de Debian és "
#~ "<a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Ha estat traduït per %s."

#, fuzzy
#~| msgid ""
#~| "<void id=\"singular\" />This issue of Debian Weekly News was edited by "
#~| "<a href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />L'editor d'aquest número de les notícies "
#~ "setmanals de Debian és <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />L'editor d'aquest número de les notícies "
#~ "setmanals de Debian és <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Ha estat traduït per %s."

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Esmenar&nbsp;una&nbsp;proposta"

#~ msgid "Amendment Proposer"
#~ msgstr "Proponent de l'esmena"

#~ msgid "Amendment Proposer A"
#~ msgstr "Proponent de l'esmena A"

#~ msgid "Amendment Proposer B"
#~ msgstr "Proponent de l'esmena B"

#~ msgid "Amendment Seconds"
#~ msgstr "Els que secunden l'esmena"

#~ msgid "Amendment Seconds A"
#~ msgstr "Els que secunden l'esmena A"

#~ msgid "Amendment Seconds B"
#~ msgstr "Els que secunden l'esmena B"

#~ msgid "Amendment Text"
#~ msgstr "Text de l'esmena"

#~ msgid "Amendment Text A"
#~ msgstr "Text de l'esmena A"

#~ msgid "Amendment Text B"
#~ msgstr "Text de l'esmena B"

#~ msgid "Amendments"
#~ msgstr "Esmenes"

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Tornar a la <a href=\"./\">pàgina de consultors de Debian</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Tornar a la <a href=\"./\">pàgina de conferenciants de Debian</a>."

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Tornar a: altres <a href=\"./\">notícies de Debian</a> || la <a href="
#~ "\"m4_HOME/\">pàgina principal del projecte Debian</a>."

#~ msgid "Ballot"
#~ msgstr "Butlleta"

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Compra CD o DVD"

#~ msgid "Choices"
#~ msgstr "Opcions"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG PMF"

#~ msgid "DLS Index"
#~ msgstr "Index DLS"

#~ msgid "Data and Statistics"
#~ msgstr "Dades i estadístiques"

#~ msgid "Date"
#~ msgstr "Data"

#~ msgid "Date published"
#~ msgstr "Data de publicació"

#~ msgid "Debate"
#~ msgstr "Debat"

#~ msgid "Debian CD team"
#~ msgstr "Equip de CD de Debian"

#~ msgid "Debian Involvement"
#~ msgstr "Participació de Debian"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Arxiu de debian-legal"

#~ msgid "Decided"
#~ msgstr "Decidida"

#~ msgid "Discussion"
#~ msgstr "Discussió"

#~ msgid "Download calendar entry"
#~ msgstr "Descarrega l'entrada del calendari"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Descàrrega per HTTP/FTP"

#~ msgid "Download with Jigdo"
#~ msgstr "Descàrrega amb Jigdo"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "<a href=\"/MailingLists/disclaimer\">Llista de correu publica</a> en "
#~ "anglès per a CDs/DVDs:"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Donar&nbsp;suport&nbsp;a&nbsp;una&nbsp;proposta"

#~ msgid "Forum"
#~ msgstr "Fòrum"

#~ msgid "Free"
#~ msgstr "Lliure"

#~ msgid "Have you found a problem with the site layout?"
#~ msgstr "Heu trobat algun problema amb el disseny del lloc web?"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Pàgina&nbsp;principal&nbsp;de&nbsp;votacions"

#~ msgid "How&nbsp;To"
#~ msgstr "Com"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "En&nbsp;discussió"

#~ msgid "Justification"
#~ msgstr "Justificació"

#~ msgid "Latest News"
#~ msgstr "Darreres notícies"

#~ msgid "License"
#~ msgstr "Llicència"

#~ msgid "License Information"
#~ msgstr "Informació de llicències"

#~ msgid "License text"
#~ msgstr "Text de la llicència"

#~ msgid "License text (translated)"
#~ msgstr "Text de la llicència (traduït)"

#~ msgid "List of Consultants"
#~ msgstr "Llista de consultors"

#~ msgid "List of Speakers"
#~ msgstr "Llista de conferenciants"

#~ msgid "Main Coordinator"
#~ msgstr "Coordinador principal"

#~ msgid "Majority Requirement"
#~ msgstr "Requeriments de majoria"

#~ msgid "Minimum Discussion"
#~ msgstr "Discussió mínima"

#~ msgid "More Info"
#~ msgstr "Més informació"

#~ msgid "More information"
#~ msgstr "Més informació"

#~ msgid "More information:"
#~ msgstr "Més informació:"

#~ msgid "Network Install"
#~ msgstr "Instal·lació per xarxa"

#~ msgid "No Requested packages"
#~ msgstr "Sense paquets demanats"

#~ msgid "No help requested"
#~ msgstr "Sense demandes d'ajuda"

#~ msgid "No orphaned packages"
#~ msgstr "Sense paquets deixats orfes"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Sense paquets esperant ser adoptats"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Sense paquets esperant ser empaquetats"

#~ msgid "No requests for adoption"
#~ msgstr "Sense demandes d'adopció"

#~ msgid "Nobody"
#~ msgstr "Ningú"

#~ msgid "Nominations"
#~ msgstr "Nominacions"

#~ msgid "Non-Free"
#~ msgstr "No-lliure"

#~ msgid "Not Redistributable"
#~ msgstr "No redistribuïble"

#~ msgid "Opposition"
#~ msgstr "Oposició"

#~ msgid "Original Summary"
#~ msgstr "Resum original"

#~ msgid "Other"
#~ msgstr "Altres"

#~ msgid "Outcome"
#~ msgstr "Resultat"

#~ msgid "Platforms"
#~ msgstr "Plataformes"

#~ msgid "Proceedings"
#~ msgstr "Actes"

#~ msgid "Proposal A"
#~ msgstr "Proposta A"

#~ msgid "Proposal A Proposer"
#~ msgstr "Proponent de la proposta A"

#~ msgid "Proposal A Seconds"
#~ msgstr "Els que secunden la proposta A"

#~ msgid "Proposal B"
#~ msgstr "Proposta B"

#~ msgid "Proposal B Proposer"
#~ msgstr "Proponent de la proposta B"

#~ msgid "Proposal B Seconds"
#~ msgstr "Els que secunden la proposta B"

#~ msgid "Proposal C"
#~ msgstr "Proposta C"

#~ msgid "Proposal C Proposer"
#~ msgstr "Proponent de la proposta C"

#~ msgid "Proposal C Seconds"
#~ msgstr "Els que secunden la proposta C"

#~ msgid "Proposal D"
#~ msgstr "Proposta D"

#~ msgid "Proposal D Proposer"
#~ msgstr "Proponent de la proposta D"

#~ msgid "Proposal D Seconds"
#~ msgstr "Els que secunden la proposta D"

#~ msgid "Proposal E"
#~ msgstr "Proposta E"

#~ msgid "Proposal E Proposer"
#~ msgstr "Proponent de la proposta E"

#~ msgid "Proposal E Seconds"
#~ msgstr "Els que secunden la proposta E"

#~ msgid "Proposal F"
#~ msgstr "Proposta F"

#~ msgid "Proposal F Proposer"
#~ msgstr "Proponent de la proposta F"

#~ msgid "Proposal F Seconds"
#~ msgstr "Els que secunden la proposta F"

#~ msgid "Proposer"
#~ msgstr "Proponent"

#~ msgid "Quorum"
#~ msgstr "Quòrum"

#~ msgid "Rating:"
#~ msgstr "Puntuació:"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Llegir un resultat"

#~ msgid "Related Links"
#~ msgstr "Enllaços relacionats"

#~ msgid "Report it!"
#~ msgstr "Informa'n!"

#~ msgid "Seconds"
#~ msgstr "Els que secunden"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Vegeu la pagina d'<a href=\"./\">informació de llicències</a> per tal a "
#~ "tenir una idea dels Resums de Llicències de Debian (DLS)."

#~ msgid "Select a server near you: &nbsp;"
#~ msgstr "Tria un servidor a prop teu: &nbsp;"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Enviar&nbsp;proposta"

#~ msgid "Summary"
#~ msgstr "Resum"

#~ msgid "Taken by:"
#~ msgstr "Agafat per:"

#~ msgid "Text"
#~ msgstr "Text"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "El resum original de <summary-author/> es pot trobar en els <a href="
#~ "\"<summary-url/>\">arxius de la llista</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Aquest resum va ser preparat per <summary-author/>."

#~ msgid "Time Line"
#~ msgstr "Línia temporal"

#, fuzzy
#~| msgid ""
#~| "To receive this newsletter weekly in your mailbox, <a href=\"http://"
#~| "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~| "list</a>."
#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"http://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Per a rebre aquest butlletí setmanal en la vostra bústia, <a href="
#~ "\"m4_HOME/MailingLists/subscribe#debian-news\">subscriviu-vos a la llista "
#~ "de correu debian-news.</a>"

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"http://lists."
#~ "debian.org/debian-news/\">subscribe to the debian-news mailing list</a>."
#~ msgstr ""
#~ "Per a rebre aquest butlletí setmanal en la vostra bústia, <a href="
#~ "\"m4_HOME/MailingLists/subscribe#debian-news\">subscriviu-vos a la llista "
#~ "de correu debian-news.</a>"

#~ msgid "Upcoming Attractions"
#~ msgstr "Properes atraccions"

#~ msgid "Version"
#~ msgstr "Versió"

#~ msgid "Visit the site sponsor"
#~ msgstr "Visiteu el lloc del patrocinador"

#~ msgid "Vote"
#~ msgstr "Votar"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Votació&nbsp;en&nbsp;curs"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "En&nbsp;espera&nbsp;de&nbsp;patrocinadors"

#~ msgid "When"
#~ msgstr "Quan"

#~ msgid "Where"
#~ msgstr "On"

#~ msgid "Withdrawn"
#~ msgstr "Retirada"

#~ msgid "buy"
#~ msgstr "compra"

#~ msgid "debian_on_cd"
#~ msgstr "debian_en_cd"

#~ msgid "free"
#~ msgstr "lliure"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "in adoption since today."
#~ msgstr "en adopció des de avui."

#~ msgid "in adoption since yesterday."
#~ msgstr "en adopció des de ahir."

#~ msgid "in preparation since today."
#~ msgstr "en preparació des de avui."

#~ msgid "in preparation since yesterday."
#~ msgstr "en preparació des de ahir."

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "link may no longer be valid"
#~ msgstr "potser l'enllaç ja no és vàlid"

#~ msgid "net_install"
#~ msgstr "instal·lació_per_xarxa"

#~ msgid "non-free"
#~ msgstr "no-lliure"

#~ msgid "not redistributable"
#~ msgstr "no redistribuïble"

#~ msgid "package info"
#~ msgstr "informació del paquet"

#~ msgid "requested %s days ago."
#~ msgstr "demanat %s dies enrere."

#~ msgid "requested today."
#~ msgstr "demanat avui."

#~ msgid "requested yesterday."
#~ msgstr "demanat ahir."
