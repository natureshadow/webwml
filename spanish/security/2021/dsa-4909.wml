#use wml::debian::translation-check translation="022e55922f28ad2d0ab4d75ed4d28a027bafddfa"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en BIND, una implementación de
servidor DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25214">CVE-2021-25214</a>

    <p>Greg Kuechle descubrió que una transferencia IXFR entrante mal construida
    podía desencadenar un fallo de aserción en named, dando lugar a denegación
    de servicio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25215">CVE-2021-25215</a>

    <p>Siva Kakarla descubrió que named podía sufrir una caída cuando un registro DNAME
    ubicado en la sección ANSWER durante el seguimiento de un DNAME resulta ser
    la respuesta final a la consulta de un cliente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25216">CVE-2021-25216</a>

    <p>Se descubrió que la implementación de SPNEGO utilizada por BIND tiene
    propensión a una vulnerabilidad de desbordamiento de memoria. Esta actualización hace que
    se use la implementación de SPNEGO de las bibliotecas de Kerberos en su lugar.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 1:9.11.5.P4+dfsg-5.1+deb10u5.</p>

<p>Le recomendamos que actualice los paquetes de bind9.</p>

<p>Para información detallada sobre el estado de seguridad de bind9, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4909.data"
