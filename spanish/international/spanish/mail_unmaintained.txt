Hola,

Este es un mensaje automático que lista las traducciones «huérfanas»,
o sea, sin mantenedor oficial.

Si tiene dudas sobre este mensaje, por favor dirija sus preguntas
a la lista debian-l10n-spanish@lists.debian.org.
