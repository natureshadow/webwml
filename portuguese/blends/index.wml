#use wml::debian::template title="Debian Pure Blends" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>
Os Debian Pure Blends são uma solução para grupos de pessoas com necessidades
específicas.
Não só eles fornecem coleções úteis (metapacotes) de pacotes específicos,
como também facilitam a instalação e a configuração para determinado
propósito.
Eles cobrem os interesses de diferentes grupos de pessoas, que podem ser
crianças, cientistas, jogadores(as), advogados(as), equipes médicas, pessoas
com deficiência visual, etc.
O objetivo geral é simplificar a instalação e a administração de computadores
para sua audiência alvo e conectar esse público com as pessoas que escrevem ou
empacotam o software que eles(as) usam.
</p>

<p>Você pode ler mais sobre os Debian Pure Blends no <a
href="https://blends.debian.org/blends/">Manual Pure Blends</a>.</p>

<ul class="toc">
<li><a href="#released">Pure Blends lançados</a></li>
<li><a href="#unreleased">Pure Blends que estão por vir</a></li>
<li><a href="#development">Desenvolvimento</a></li>
</ul>

<div class="card" id="released">
<h2>Pure Blends lançados</h2>
<div>
<p>"Lançados" pode ter significados diferentes para Blends diferentes. Na
maior parte dos casos, significa que o blend tem metapacotes ou um instalador
que foi lançado em uma versão estável do Debian. Os blends também podem
oferecer mídias de instalação ou podem formar a base de uma distribuição
derivada. Veja as páginas individuais dos blends para mais informações
sobre aquele blend particular.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Descrição</th>
  <th>Links rápidos</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>Pure Blends que estão por vir</h2>
  <div>
   <p>Esses blends estão sendo construídos e ainda não tiveram uma versão
   estável, embora eles estejam disponíveis na versão <a
href="https://www.debian.org/releases/testing/">teste (testing)</a> ou <a
href="https://www.debian.org/releases/unstable">instável (unstable)</a>.
Alguns Pure Blends que ainda serão lançados talvez possam depender de
componentes não livres.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Blend</th>
  <th>Descrição</th>
  <th>Links rápidos</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>

 <div class="card" id="development">
  <h2>Desenvolvimento</h2>
  <div>
   <p>Se você está interessado(a) em se envolver no desenvolvimento de
Debian Pure Blends, você pode encontrar informações de desenvolvimento nas
<a href="https://wiki.debian.org/DebianPureBlends">páginas da wiki</a>.
   </p>
  </div>
</div><!-- #main -->