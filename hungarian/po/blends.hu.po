msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Metacsomagok"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Letöltések"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Származékok"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"A Debian Astro célja egy olyan Debian alapú operációs rendszer "
"kifejlesztéseami egyarán megfelel profi és hobbi csillagászoknak. Nagy számú "
"szoftver csomagottartalmaz, melyek lefedik a teleszkóp vezérlés, az "
"adatcsökkentés, az előadás és más területek szükségleteit."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"A DebiChem célja egy jó Debian alapú platform létrehozása kémikusok napi "
"munkájához."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"A Debian Games célja, hogy játékokat biztossítson a Debian-ban, az arcade "
"típusú és kalandjátékoktól egészen a szimulációig és stratégiáig."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"A Debian Edu célja oktatáshoz és iskolában is használhstó Debian alapú "
"operációs rendszer megalkotása."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""
"A Debian GIS célja a Debian-t földrazi információs rendszerek "
"alkalmazásokhoz legjobban használható disztribúcióvá fejleszteni."

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"A Debian Junior célja a Debian-t olyan operációs rendszerré tenni, amit "
"gyerekek isélvezhetnek."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"A Debian Med célja egy teljesen szabad és nyílt rendszer kialakítása az "
"orvosi kezelésben és kutatásban. Ennek a célnak az eléréséhez a Debian Med "
"tartalmaz szabad és nyílt forrású programokat orvosi képalkotáshoz, "
"bioinformatikához, klinikai IT infrastruktúrához és másokat, a Debian "
"operációs rendszeren belül."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"A Debian Multimedia célja, hogy a Debiant jól használató platformmá tegye "
"zenei és multimédiás munkához."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"A Debain Science cékja, hogy jobb élményt nyújtson kutatóknak és tudósoknak "
"a Debian használata során."

#: ../../english/blends/released.data:89
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"A FreedomBox célja kifejleszteni, tervezni és népszerűsíteni szabad "
"szoftverek futtatását szervereken magán célra, személyes kommunikációra. "
"Olyan alkalmazásokkal, mint blogok, wiki, weboldalak, közösségi hálózatok, "
"email, web proxy és Tor relay egy olyan eszközön, ami helyettesíteni tud egy "
"router-t, vagyis az adatok a felhasználónál maradnak."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"A Debian Accessibility célja, hogy a Debiant olyan operációs rendszerré "
"tegye, ami különösen jól alkazhazkodik a fogyatékkal élők elvárásaihoz."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"A Debian Desing célja, hogy alkalmazásokat biztosítson dizájnereknek. Ide "
"tartoznak a grafikusok, web dizájnerek és multimédia dizájnerek."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""
"A Debian EzGo célja olyan kultúrális alapú nyílt és szabad technológia "
"nyújtása, anyanyelvi támogatással és megfelelően könnyű kezelhetőséggel, "
"kicsi és gyors asztali környezettel az alacsony teljesítményű/árú "
"hardverekhez, hogy a Debian használatával elősegítse az emberi képességek "
"építését és a technológiai fejlődést különböző területeken és régiókban, "
"mint Afrika, Afganisztán, Inonézia, Vietnám."

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"A Debian Hamradio célja a rádióamatőrök szükségleteinek kielégítése "
"Debiannal, naplózási, adat és csomag módú és egyéb alkalmazások nyújtásával."

#: ../../english/blends/unreleased.data:47
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"A DebianParl célja alkalmazások biztosítása a parlamenti képviselők, "
"politikusok és a csapataik igényeinek megfelelően."
