#use wml::debian::translation-check translation="17d1d33a4ee43a02a589137e490f76fb1ff4dac3" maintainer="Szabolcs Siebenhofer"
<define-tag description>LTS biztonsági frissítés</define-tag>
<define-tag moreinfo>
<p>Néhány sebezhetőéget fedeztek fel a spice-vdagent-ben, ami egy spice 
guest agent, a SPICE integrációjának javítása és az élmény fokozása miatt. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15108">CVE-2017-15108</a>

    <p>A spice-vdagent nem megfelelően semlegesíti a mentési könyvtárat, 
    mielőtt átadja a shell-nek, amivel lehetővé teszi, hogy a helyi 
    támadó hozzáférjen az agent által futtatott munkamenethez, amiben 
    bármilyen tetszőleges kódot le tud futtatni.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25650">CVE-2020-25650</a>

    <p>Hibát találtak abban, ahogy a spice-vdagentd a fájl átviteleket
    kezelte a host rendszer és a virtuális gép között. Bármely 
    privilegizálatlan helyi vendég felhasználó, akinek hozzáférése volt a
    `/run/spice-vdagentd/spice-vdagent-sock` elérési úthoz, képes volt
    a spice-vdagentd-ben memória szolgáltatás megtagadást okozni, 
    vagy akár a virtuális gép más folyamataiban. Ez a rés legjobban
    a rendszer elérhetőségét fenyegeti. Ez a hiba a 0.20 és az előtti 
    verziókat érinti.
    </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25651">CVE-2020-25651</a>

    <p>Hibát találtak a SPICE fájlátviteli protokollban. A gazda rendszerből
    származó adatok teljes egészében vagy részekben kerülhetnek egy 
    jogosulatlan felhasználó kliens kapcsolatában a VM rendszerben. Más
    felhasználóktól származó aktív fájlátvitel megszakadhat, ami szolgáltatás
    megtagadást eredményezhet. A sérülékenység legjobban az adatok
    biztonságát és a rendszer elérhetősége veszélyezteti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25652">CVE-2020-25652</a>

    <p>Hibát találtak a spice-vdagentd démonban, ahol az nem megfelelően
    kezelte a `/run/spice-vdagentd/spice-vdagent-sock` UNIX domain socketen
    keresztüli kliens kapcsolatokat. Bármely jogosulatlan helyi felhasználó
    megakadályozhatja, hogy jogosult agent-ek csatlakozzanak a 
    spice-vdagentd démonhoz, ezzel szolgáltatás megtagadást okozva.
    A hiba legjobban a rendszer elérhetőségét veszélyezteti.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25653">CVE-2020-25653</a>

    <p>Versenyfeltétel sérülékenységet találtak abban, ahogy a spice-vdagentd
    az új kliens kapcsolatokat kezelte. Ez a hiba lehetővé teszi, hogy
    jogosulatlan helyi vendég felhasználó a spice-vdagentd agent-évé váljon,
    ami szolgáltatás megtagadást vagy a szerverről információszivárgást 
    okozhat. A sebezhetőség legjobban az adatok titkosságát, valamint a 
    rendszer elérhetőségét veszélyezteti.</p></li>

</ul>

<p>A Debian 9 <q>stretch</q> esetén a probléma a 0.17.0-1+deb9u1 verzióban 
javításra került.</p>

<p>Azt tanácsoljuk, hogy frissítsd az spice-vdagent csomagjaidat.</p>

<p>Az spice-vdagent csomag biztonság állapotával kapcsolatban lásd a bizonsági 
nyomkövető oldalát:
<a href="https://security-tracker.debian.org/tracker/spice-vdagent">https://security-tracker.debian.org/tracker/spice-vdagent</a></p>

<p>További információk a Debian LTS biztonági figyelmeztetéseiről, hogyan tudod ezeket a 
frissítéseket a rendszereden telepíteni és más gyakran feltett kérdések megtalálhatóak itt: 
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2524.data"
# $Id: $
