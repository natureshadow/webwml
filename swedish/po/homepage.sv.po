msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Andreas Rönnquist <andreas@ronnquist.net>\n"
"Language-Team: \n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Det universella operativsystemet"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "DC19 Gruppfoto"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "DebConf19 Gruppfoto"

#: ../../english/index.def:19
msgid "Mini DebConf Regensburg 2021"
msgstr "Mini DebConf Regensburg 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Gruppfoto av MiniDebConf i Regensburg 2021"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Skärmdump från installationsprogrammet Calamares"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Skärmdump från installationsprogrammet Calamares"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian är som en Schweizisk armékniv"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "Folk har roligt med Debian"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Debianfolk på Debconf18 i Hsinchu som har riktigt kul"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Bits från Debian"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blogg"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Mikronyheter"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Mikronyheter från Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planet"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "Debians planet"
