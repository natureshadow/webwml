#use wml::debian::translation-check translation="bfb9d0ec9052de9c60ec367a3a8c7c7091f3e49d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans XStream, il existait une vulnérabilité qui pouvait permettre à un
attaquant distant de charger et d’exécuter du code arbitraire à partir d’un
hôte distant simplement en manipulant le flux d’entrée traité.</p>

<p>Les hiérarchies de types pour java.io.InputStream, java.nio.channels.Channel,
javax.activation.DataSource et javax.sql.rowsel.BaseRowSet sont désormais
mises en liste noire ainsi que les types particuliers
com.sun.corba.se.impl.activation.ServerTableEntry,
com.sun.tools.javac.processing.JavacProcessingEnvironment$NameProcessIterator,
sun.awt.datatransfer.DataTransferer$IndexOrderComparator et
sun.swing.SwingLazyValue. De plus le type interne
Accessor$GetterSetterReflection de JAXB, les types internes
MethodGetter$PrivilegedGetter et ServiceFinder$ServiceNameIterator de JAX-WS,
toutes les classes internes de javafx.collections.ObservableList et un chargeur
de classe interne utilisé dans une copie privée BCEL font désormais partie
de la liste noire par défaut et la désérialisation de XML contenant un de ces
types échouera. Vous devez autoriser ces types à l’aide d’une configuration
explicite si vous en avez besoin.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.4.11.1-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxstream-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxstream-java, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">\
https://security-tracker.debian.org/tracker/libxstream-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2616.data"
# $Id: $
