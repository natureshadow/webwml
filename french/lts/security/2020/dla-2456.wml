#use wml::debian::translation-check translation="808eced345149e59e8db57258d450d97cde9dbf3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Python.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

<p>Dans Lib/tarfile.py, un attaquant peut élaborer une archive TAR conduisant
à une boucle infinie lorsqu’elle est ouverte par tarfile.open, à cause d’une
validation manquante d’en-tête _proc_pax</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26116">CVE-2020-26116</a>

<p>http.client permet une injection CRLF si l’attaquant contrôle la méthode
de requête HTTP.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.3-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.5, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2456.data"
# $Id: $
