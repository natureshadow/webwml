#use wml::debian::translation-check translation="35405b68aa31c415947c277663980744599dc1e0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21227">CVE-2021-21227</a>

<p>Gengming Liu a découvert un problème de validation de données dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21228">CVE-2021-21228</a>

<p>Rob Wu a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21229">CVE-2021-21229</a>

<p>Mohit Raj a découvert une erreur dans l'interface utilisateur du
téléchargement de fichier.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21230">CVE-2021-21230</a>

<p>Manfred Paul a découvert l'utilisation d'un type incorrect.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21231">CVE-2021-21231</a>

<p>Sergei Glazunov a découvert un problème de validation de données dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21232">CVE-2021-21232</a>

<p>Abdulrahman Alqabandi a découvert un problème d'utilisation de mémoire
après libération dans les outils de développement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21233">CVE-2021-21233</a>

<p>Omair a découvert un problème de dépassement de tampon dans la
bibliothèque ANGLE.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 90.0.4430.93-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4911.data"
# $Id: $
