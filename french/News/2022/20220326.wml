#use wml::debian::translation-check translation="d673db021e55bd42a14712c110ed1253cd2c8b1e" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.3</define-tag>
<define-tag release_date>2022-03-26</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la troisième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction apache-log4j1.2 "Problèmes de sécurité [CVE-2021-4104 CVE-2022-23302 CVE-2022-23305 CVE-2022-23307] résolus en supprimant la prise en charge des modules JMSSink, JDBCAppender, JMSAppender et Apache Chainsaw">
<correction apache-log4j2 "Correction d'un problème d'exécution de code à distance [CVE-2021-44832]">
<correction apache2 "Nouvelle version amont ; correction d'un plantage dû à une lecture aléatoire de mémoire [CVE-2022-22719] ; correction d'un problème de dissimulation de requête HTTP [CVE-2022-22720] ; corrections de problèmes de lecture hors limites [CVE-2022-22721 CVE-2022-23943]">
<correction atftp "Correction d'un problème de fuite d'informations [CVE-2021-46671]">
<correction base-files "Mise à jour pour cette version 11.3">
<correction bible-kjv "Correction d'une erreur due à un décalage d'entier dans la recherche">
<correction chrony "Lecture permise du fichier de configuration de chronyd que génère timemaster(8)">
<correction cinnamon "Correction d'un plantage lors de l'ajout d'un compte en ligne avec connexion">
<correction clamav "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2022-20698]">
<correction cups-filters "Apparmor : lecture permise à partir du fichier de configuration de cups-browsed dans Debian Edu">
<correction dask.distributed "Correction de l'écoute non souhaitée de <q>>workers</q>> sur les interfaces publiques [CVE-2021-42343] ; correction de la compatibilité avec Python 3.9">
<correction debian-installer "Reconstruction avec proposed-updates ; mise à jour de l'ABI du noyau vers la version 5.10.0-13">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-ports-archive-keyring "Ajout de la <q>clé de signature automatique de l'archive des portages de Debian (2023)</q> ; migration de la clé de 2021 dans le trousseau retiré">
<correction django-allauth "Correction de la prise en charge d'OpenID">
<correction djbdns "Limite des données de axfrdns, dnscache et tinydns réhaussée">
<correction dpdk "Nouvelle version amont stable">
<correction e2guardian "Correction d'un problème de certificat SSL manquant [CVE-2021-44273]">
<correction epiphany-browser "Contournement d'un bogue dans la GLib, corrigeant un plantage du processus de l'interface utilisateur">
<correction espeak-ng "Suppression du délai infondé de 50 ms durant le traitement des événements">
<correction espeakup "debian/espeakup.service : protection d'espeakup contre les surcharges du système">
<correction fcitx5-chinese-addons "fcitx5-table : ajout de dépendances manquantes à fcitx5-module-pinyinhelper et fcitx5-module-punctuation">
<correction flac "Correction d'un problème de lecture hors limites [CVE-2021-0561]">
<correction freerdp2 "Désactivation de la journalisation supplémentaire de débogage">
<correction galera-3 "Nouvelle version amont">
<correction galera-4 "Nouvelle version amont">
<correction gbonds "Utilisation de l'API Treasury pour les données de remboursement">
<correction glewlwyd "Correction d'une possible élévation de privilèges">
<correction glibc "Correction d'une conversion incorrecte à partir d'ISO-2022-JP-3 avec iconv [CVE-2021-43396] ; correction de problèmes de dépassement de tampon [CVE-2022-23218 CVE-2022-23219] ; correction d'un problème d'utilisation de mémoire après libération [CVE-2021-33574] ; arrêt du remplacement des versions plus anciennes de /etc/nsswitch.conf ; simplification de la vérification des versions du noyau prises en charge dans la mesure où les noyaux 2.x ne sont plus pris en charge ; prise en charge de l'installation des noyaux avec un numéro supérieur à 255">
<correction glx-alternatives "Après la configuration initiale des détournements, installation d'une alternative minimale aux fichiers détournés afin que des bibliothèques ne soient pas manquantes jusqu'à ce que glx-alternative-mesa traite ses déclenchements">
<correction gnupg2 "scd : Correction du pilote CCID pour SCM SPR332/SPR532 ; interaction réseau évitée dans generator qui peut mener à des blocages">
<correction gnuplot "Correction d'une division par zéro [CVE-2021-44917]">
<correction golang-1.15 "Correction d'IsOnCurve pour les valeurs big.Int qui n'ont pas de coordonnées valables [CVE-2022-23806] ; math/big : grosse consommation de mémoire évitée dans Rat.SetString [CVE-2022-23772] ; cmd/go : pas de matérialisation des branches en versions [CVE-2022-23773] ; correction d'épuisement de pile lors de la compilation d'expressions profondément imbriquées [CVE-2022-24921]">
<correction golang-github-containers-common "Mise à jour de la prise en charge de seccomp pour activer l'utilisation des dernières versions du noyau">
<correction golang-github-opencontainers-specs "Mise à jour de la prise en charge de seccomp pour activer l'utilisation des dernières versions du noyau">
<correction gtk+3.0 "Correction de résultats de recherche manquants lors de l'utilisation de NFS ; verrouillage de la gestion du presse-papier de Wayland évité dans certains cas particuliers ; amélioration de l'impression sur les imprimantes découvertes par mDNS">
<correction heartbeat "Correction de la création de /run/heartbeat sur les systèmes utilisant systemd">
<correction htmldoc "Correction d'un problème de lecture hors limites [CVE-2022-0534]">
<correction installation-guide "Mise à jour de la documentation et des traductions">
<correction intel-microcode "Mise en jour du microprogramme inclus ; atténuation de certains problèmes de sécurité [CVE-2020-8694 CVE-2020-8695 CVE-2021-0127 CVE-2021-0145 CVE-2021-0146 CVE-2021-33120]">
<correction ldap2zone "Utilisation de <q>mktemp</q> à la place de <q>tempfile</q> obsolète, évitant des avertissements">
<correction lemonldap-ng "Correction du processus d'authentification dans les greffons de test de mot de passe [CVE-2021-40874]">
<correction libarchive "Correction de l'extraction de liens directs en liens symboliques ; correction de la gestion des ACL des liens symboliques [CVE-2021-23177] ; liens symboliques non suivis lors de la configuration des attributs de fichier [CVE-2021-31566]">
<correction libdatetime-timezone-perl "Mise à jour des données incluses">
<correction libgdal-grass "Reconstruction avec grass 7.8.5-1+deb11u1">
<correction libpod "Mise à jour de la prise en charge de seccomp pour activer l'utilisation des dernières versions du noyau">
<correction libxml2 "Correction d'un problème d'utilisation de mémoire après libération [CVE-2022-23308]">
<correction linux "Nouvelle version amont stable ; [rt] mise à jour vers la version 5.10.106-rt64; passage de l'ABI à la version 13">
<correction linux-signed-amd64 "Nouvelle version amont stable ; [rt] mise à jour vers la version 5.10.106-rt64 ; passage de l'ABI à la version 13">
<correction linux-signed-arm64 "Nouvelle version amont stable ; [rt] mise à jour vers la version 5.10.106-rt64 ; passage de l'ABI à la version 13">
<correction linux-signed-i386 "Nouvelle version amont stable ; [rt] mise à jour vers la version 5.10.106-rt64 ; passage de l'ABI à la version 13">
<correction mariadb-10.5 "Nouvelle version amont stable ; corrections de sécurité [CVE-2021-35604 CVE-2021-46659 CVE-2021-46661 CVE-2021-46662 CVE-2021-46663 CVE-2021-46664 CVE-2021-46665 CVE-2021-46667 CVE-2021-46668 CVE-2022-24048 CVE-2022-24050 CVE-2022-24051 CVE-2022-24052]">
<correction mpich "Ajout du champ Break dans les versions plus anciennes de libmpich1.0-dev, résolvant certains problèmes de mise à niveau">
<correction mujs "Correction d'un problème de dépassement de tampon [CVE-2021-45005]">
<correction mutter "Rétroportage de divers correctifs de la branche stable amont">
<correction node-cached-path-relative "Correction d'un problème de pollution de prototype [CVE-2021-23518]">
<correction node-fetch "Pas de transmission d'en-têtes sécurisés aux domaines tiers [CVE-2022-0235]">
<correction node-follow-redirects "Pas d'envoi d'en-tête Cookie entre domaines [CVE-2022-0155] ; pas d'envoi d'en-têtes confidentiels entre schémas [CVE-2022-0536]">
<correction node-markdown-it "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2022-21670]">
<correction node-nth-check "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3803]">
<correction node-prismjs "Balise protégée dans les sorties de la ligne de commande [CVE-2022-23647] ; mise à jour des fichiers minimisés pour assurer que le problème de déni de service basé sur les expressions rationnelles est résolu [CVE-2021-3801]">
<correction node-trim-newlines "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-33623]">
<correction nvidia-cuda-toolkit "cuda-gdb : désactivation de la prise en charge non fonctionnelle de Python provoquant des erreurs de segmentation ; utilisation d'un instantané (<q>snapshot</q>) d'openjdk-8-jre (8u312-b07-1)">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont ; corrections de problèmes de déni de service [CVE-2022-21813 CVE-2022-21814] ; nvidia-kernel-support : fourniture de /etc/modprobe.d/nvidia-options.conf comme modèle">
<correction nvidia-modprobe "Nouvelle version amont">
<correction openboard "Correction de l'icône de l'application">
<correction openssl "Nouvelle version amont ; correction de l’authentification de pointeur d'armv8">
<correction openvswitch "Correction d'un problème d'utilisation de mémoire après libération [CVE-2021-36980] ; correction de l'installation de libofproto">
<correction ostree "Correction de compatibilité avec eCryptFS ; récursion infinie évitée lors de la récupération de certaines erreurs ; commits marqués comme partiels avant téléchargement ; correction d'un échec d'assertion lors de l'utilisation d'un rétroportage ou d'une construction locale de GLib &gt;= 2.71 ; correction de la possibilité de récupérer du contenu d'OSTree à partir de chemins contenant des caractères inconnus dans les URI (tels qu'une barre oblique inverse) ou non ASCII">
<correction pdb2pqr "Correction de la compatibilité de propka avec Python 3.8 ou supérieur">
<correction php-crypt-gpg "Passage d'option supplémentaire à GPG empêché [CVE-2022-24953]">
<correction php-laravel-framework "Correction d'un problème de script intersite [CVE-2021-43808], et de l'absence de blocage de téléchargement de contenu exécutable [CVE-2021-43617]">
<correction phpliteadmin "Correction d'un problème de script intersite [CVE-2021-46709]">
<correction prips "Correction d'une encapsulation infinie si une plage atteint 255.255.255.255 ; correction de sortie de CIDR avec des adresses dont le premier bit diffère">
<correction pypy3 "Correction d'échecs de construction en supprimant un #endif superflu de import.h">
<correction python-django "Correction d'un problème de déni de service [CVE-2021-45115], d'un problème de divulgations d'informations [CVE-2021-45116], d'un problème de traversée de répertoires [CVE-2021-45452] ; correction d'une trace d'appel autour de la gestion de RequestSite/get_current_site() due à une importation circulaire">
<correction python-pip "Situation de compétition évitée lors de l'utilisation de dépendances importées par zip">
<correction rust-cbindgen "Nouvelle version amont stable pour prendre en charge la construction des dernières versions de firefox-esr et de thunderbird">
<correction s390-dasd "Plus de transmission de l'option obsolète -f à dasdfmt">
<correction schleuder "Migration de valeurs booléennes en entiers, si l'adaptateur de connexion ActiveRecord de SQLite3 est utilisé, rétablissant la fonctionnalité">
<correction sphinx-bootstrap-theme "Correction de la fonction de recherche">
<correction spip "Correction de plusieurs problèmes de script intersite">
<correction symfony "Correction d'un problème d'injection de CVE [CVE-2021-41270]">
<correction systemd "Correction d'une récursion non contrôlée dans systemd-tmpfiles [CVE-2021-3997] ; passage de systemd-timesyncd de <q>Depends</q> à <q>Recommends</q>, supprimant un cycle de dépendances ; correction d'un échec de montage avec l'option bind d'un répertoire dans un conteneur avec machinectl ; correction d'une régression dans udev provoquant de longs délais lors du traitement de partitions portant la même étiquette ; correction d'une régression lors de l'utilisation de systemd-networkd dans un conteneur LXD non privilégié">
<correction sysvinit "Correction de l'analyse de <q>shutdown +0</q> ; clarification du fait que lorsqu'il est appelé avec <q>heure</q> shutdown ne quittera pas">
<correction tasksel "Installation de CUPS pour toutes les tâches *-desktop dans la mesure où task-print-service n'existe plus">
<correction usb.ids "Mise à jour des données incluses">
<correction weechat "Correction d'un problème de déni de service [CVE-2021-40516]">
<correction wolfssl "Correction de plusieurs problèmes liés à la gestion d'OCSP [CVE-2021-3336 CVE-2021-37155 CVE-2021-38597] et à la prise en charge de TLS1.3 [CVE-2021-44718 CVE-2022-25638 CVE-2022-25640]">
<correction xserver-xorg-video-intel "Correction d'un plantage de SIGILL avec les processeurs non SSE2">
<correction xterm "Correction d'un problème de dépassement de tampon [CVE-2022-24130]">
<correction zziplib "Correction d'un problème de déni de service [CVE-2020-18442]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 5000 openjdk-11>
<dsa 2021 5001 redis>
<dsa 2021 5012 openjdk-17>
<dsa 2021 5021 mediawiki>
<dsa 2021 5023 modsecurity-apache>
<dsa 2021 5024 apache-log4j2>
<dsa 2021 5025 tang>
<dsa 2021 5027 xorg-server>
<dsa 2021 5028 spip>
<dsa 2021 5029 sogo>
<dsa 2021 5030 webkit2gtk>
<dsa 2021 5031 wpewebkit>
<dsa 2021 5033 fort-validator>
<dsa 2022 5035 apache2>
<dsa 2022 5037 roundcube>
<dsa 2022 5038 ghostscript>
<dsa 2022 5039 wordpress>
<dsa 2022 5040 lighttpd>
<dsa 2022 5041 cfrpki>
<dsa 2022 5042 epiphany-browser>
<dsa 2022 5043 lxml>
<dsa 2022 5046 chromium>
<dsa 2022 5047 prosody>
<dsa 2022 5048 libreswan>
<dsa 2022 5049 flatpak-builder>
<dsa 2022 5049 flatpak>
<dsa 2022 5050 linux-signed-amd64>
<dsa 2022 5050 linux-signed-arm64>
<dsa 2022 5050 linux-signed-i386>
<dsa 2022 5050 linux>
<dsa 2022 5051 aide>
<dsa 2022 5052 usbview>
<dsa 2022 5053 pillow>
<dsa 2022 5054 chromium>
<dsa 2022 5055 util-linux>
<dsa 2022 5056 strongswan>
<dsa 2022 5057 openjdk-11>
<dsa 2022 5058 openjdk-17>
<dsa 2022 5059 policykit-1>
<dsa 2022 5060 webkit2gtk>
<dsa 2022 5061 wpewebkit>
<dsa 2022 5062 nss>
<dsa 2022 5063 uriparser>
<dsa 2022 5064 python-nbxmpp>
<dsa 2022 5065 ipython>
<dsa 2022 5067 ruby2.7>
<dsa 2022 5068 chromium>
<dsa 2022 5070 cryptsetup>
<dsa 2022 5071 samba>
<dsa 2022 5072 debian-edu-config>
<dsa 2022 5073 expat>
<dsa 2022 5075 minetest>
<dsa 2022 5076 h2database>
<dsa 2022 5077 librecad>
<dsa 2022 5078 zsh>
<dsa 2022 5079 chromium>
<dsa 2022 5080 snapd>
<dsa 2022 5081 redis>
<dsa 2022 5082 php7.4>
<dsa 2022 5083 webkit2gtk>
<dsa 2022 5084 wpewebkit>
<dsa 2022 5085 expat>
<dsa 2022 5087 cyrus-sasl2>
<dsa 2022 5088 varnish>
<dsa 2022 5089 chromium>
<dsa 2022 5091 containerd>
<dsa 2022 5092 linux-signed-amd64>
<dsa 2022 5092 linux-signed-arm64>
<dsa 2022 5092 linux-signed-i386>
<dsa 2022 5092 linux>
<dsa 2022 5093 spip>
<dsa 2022 5095 linux-signed-amd64>
<dsa 2022 5095 linux-signed-arm64>
<dsa 2022 5095 linux-signed-i386>
<dsa 2022 5095 linux>
<dsa 2022 5098 tryton-server>
<dsa 2022 5099 tryton-proteus>
<dsa 2022 5100 nbd>
<dsa 2022 5101 libphp-adodb>
<dsa 2022 5102 haproxy>
<dsa 2022 5103 openssl>
<dsa 2022 5104 chromium>
<dsa 2022 5105 bind9>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction angular-maven-plugin "Plus nécessaire">
<correction minify-maven-plugin "Plus nécessaire">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
