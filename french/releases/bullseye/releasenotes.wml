#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b" maintainer="Baptiste Jammet"
#use wml::debian::template title="Debian 11 – Notes de publication" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"

<if-stable-release release="stretch">
<p>Ceci est une <strong>version en cours d'amélioration</strong> des notes de
publication pour Debian 10, surnommée Buster, qui n'est pas encore
publiée. Les informations présentées sur cette page peuvent être inexactes et
obsolètes et sont généralement incomplètes.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Ceci est une <strong>version en cours d'amélioration</strong> des notes de
publication pour Debian 11, surnommée Bullseye, qui n'est pas encore
publiée. Les informations présentées sur cette page peuvent être inexactes et
obsolètes et sont généralement incomplètes.</p>
</if-stable-release>

<p>Pour découvrir les nouveautés présentes dans Debian 11, consultez les notes
de publication pour votre architecture :</p>

<ul>
<:= &permute_as_list('release-notes/', 'Notes de publication'); :>
</ul>

<p>Les notes de publication contiennent également des instructions pour les
utilisateurs qui mettent à jour leur système à partir des versions précédentes.</p>

<p>Si vous avez configuré correctement les options de langue de votre
navigateur, vous pouvez utiliser le lien ci-dessus pour avoir automatiquement la
bonne page HTML &mdash; voir les explications concernant la <a href="$(HOME)/intro/cn">négociation de contenu</a>.
Sinon, choisissez l'architecture adéquate, la langue et le format que vous
souhaitez dans le tableau ci-dessous.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architecture</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Langues</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
